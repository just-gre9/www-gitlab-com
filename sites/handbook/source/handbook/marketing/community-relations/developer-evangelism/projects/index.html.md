---
layout: handbook-page-toc
title: "Projects"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## everyonecancontribute.com

Maintainer: [Michael Friedrich](/company/team/#dnsmichi)

[everyonecancontribute.com](https://everyonecancontribute.com) serves as the main website for a community formed around tech coffee chats called `#everyonecancontribute cafe` and `#everyonecancontribute Kaeffchen`.

* The website is built using [GitLab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/).
* Organization happens in the GitLab group [everyonecancontribute](https://gitlab.com/groups/everyonecancontribute/-/issues).
* Communication via [Gitter](https://gitter.im/everyonecancontribute).

The weekly coffee chats come with a theme and allow to

* try out newly announced projects together
* do pair programming/debugging sessions
* start discussions and share ideas on the latest technology

The sessions are hosted by Michael Friedrich with their Zoom and Calendar invites. The website's [About page](https://everyonecancontribute.com//page/about/) covers more details including the exact date and time.

### `#everyonecancontribute cafe`

* Live stream on [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A)
* [YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp1Gni9SyudMmXmBJIp7rIc)
* [Organisation issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/3171) and [current project](https://gitlab.com/everyonecancontribute/general/-/issues?label_name%5B%5D=lang%3A%3Aen).

### `#everyonecancontribute Kaeffchen`

* Live stream on [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A)
* GitLab Unfiltered [YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko8J8V5V794CXZUZ-DLxccI).
* [Organisation issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2482) and [current project](https://gitlab.com/everyonecancontribute/general/-/issues?label_name%5B%5D=lang%3A%3Ade).

## everyonecancontribute.dev

[everyonecancontribute.dev](https://everyonecancontribute.dev) hosts a demo page with funny animations featuring the Tanuki, Clippy, and more. It is deployed in a container environment and provides a Prometheus node exporter for monitoring demos and talks. Michael created the website for the job application presentation panel at GitLab.

## Developer Evangelism Dashboard

Maintainer: [Abubakar Siddiq Ango](/company/team/#abuango)

Our [custom dashboard](https://gitlab-com.gitlab.io/marketing/corporate_marketing/technical-evangelism/code/de-dashboard/) is built using [GitLab pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) and presents an overview of Developer Evangelism issues. The requests are synced in a specified interval.

Project: [DE Dashboard](https://gitlab.com/gitlab-com/marketing/corporate_marketing/technical-evangelism/code/de-dashboard)

## Developer Evangelism Bot

Maintainer: [Abubakar Siddiq Ango](/company/team/#abuango)

Similar to the triage bot, this bot aims to automate DE Tasks such as:

* Pull social media metrics from team members into a defined spreadsheet
* Create release evangelism issues for team members on the 15th every month.
* Generate an issue letter (created, closed, open CFPs) on every Monday.

Project: [DE Bot](https://gitlab.com/gitlab-com/marketing/corporate_marketing/developer-evangelism/code/de-bot)

## Lab Work

Maintainer: [Brendan O'Leary](/company/team/#brendan)

[labwork.dev](https://labwork.dev/) is a collection of applications made to show off exciting ideas and development challenges.

* Link Shortener
* [15th Anniversary of Git](https://git15.labwork.dev/)

Project: [Lab Work](https://gitlab.com/brendan/labwork)

## Our Work Environments

* [Brendan's dotfiles](https://gitlab.com/brendan/dotfiles)
* [Michael's dotfiles](https://gitlab.com/dnsmichi/dotfiles) covered in [this blog post](https://about.gitlab.com/blog/2020/04/17/dotfiles-document-and-automate-your-macbook-setup/)
