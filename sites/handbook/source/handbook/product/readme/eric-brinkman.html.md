---
layout: markdown_page
title: "Eric Brinkman's README"
---

## Eric's README

Thanks for visiting - Eric's README has moved to: [https://gitlab.com/ebrinkman/README/-/blob/master/README.md](https://gitlab.com/ebrinkman/README/-/blob/master/README.md)
