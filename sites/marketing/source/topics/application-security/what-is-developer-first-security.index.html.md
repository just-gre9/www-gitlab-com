---
layout: markdown_page
title: "What is developer-first security?"
description: "Want better code quality and happier developers? Developer-first security puts vulnerability discovery and remediation in an existing IDE. Your security mind blown."
---

DevSecOps is a software development methodology designed to bring development, security and operations together on a single unified team. Application security has long been an afterthought in the software development process and a core vision of DevSecOps is to shift security left, i.e., much closer to development, than ever before. The concept of developer-first security is a relatively new process that could represent the ultimate security shift-left: putting security tools in the hands of a developer so that a large portion of security scanning, testing and remediation actually happen within a dev’s IDE (integrated development environment). 

## Why application security matters

A recent Forrester Research survey of security professionals found that fully one-third of them had experienced a security breach. And it’s important to realize [the code is now the primary target](/blog/2020/10/14/why-security-champions/), rather than the infrastructure. Making things even trickier, some estimates suggest close to <a href="https://www.helpnetsecurity.com/2018/05/22/open-source-code-security-risk/" target="_blank">60% of applications are made up of open source code</a> – and others put those estimates as high as 80% or 90%. Open source code is something that’s inherently more vulnerable than code generated from scratch, but it’s an understandable choice for busy developers trying to meet ever-tightening deadlines.

## The traditional approach to security

For years, security was part of a separate organization known to swoop in after the code was committed, find problems and demand changes from (perhaps not surprisingly) reluctant developers who’d already moved on to the next project. Security was not just an afterthought; it was a top-down experience delivered by people who were far removed from the challenges of development. It’s not at all hard to understand why this approach was a major source of frustration for everyone involved.

## Enter DevSecOps

The goal of DevSecOps was to build on the silo-busting that happened when DevOps was implemented – now dev, ops, and security all work together. It’s still early days, of course, but our 2020 [Global DevSecOps Survey](/developer-survey/) showed promising signs: Almost 28% of security professionals said they’re part of a *cross-functional* security team for the first time, while nearly as many said they’re now part of the development process.

But serious friction between developers and security remained. For the second year in a row, our survey found a sizable majority of security pros continued to complain that developers found far too few bugs and that those they did find were discovered too late in the process. They also said it was difficult to get devs to actually fix the problem.

From the developer side, the issue was clear: Most companies weren’t running many, or any, security scans, and for those that did, just a fraction gave devs access to the results within their coding environment. No one likes context-switching, but for developers [it can be especially challenging](/blog/2020/08/13/developer-security-divide/) and lead to “out of sight, out of mind” outcomes.

## Developer-first (or in-context) security

To break what feels like a very vicious cycle, experts say it’s time to start thinking about in-context or developer-first security. In a nutshell, dev-first security gives a coder a “developer-friendly” security tool that lives in the IDE and empowers finding and fixing bugs in a painless manner. Ideally the security portion will be something that’s automated, allowing a busy developer to not have to think about bugs; the process will just happen naturally as part of the coding process. 

Key to the success of developer-first security is a change in perspectives on both sides. Security pros need to remember devs wear a lot of hats (coding, testing, security, and even some ops functions) while they are able to focus solely on vulnerabilities. Given that, it’s vital that security pros spend time understanding what developers are asked to do – and perhaps learn to code? – in order to provide the necessary training, encouragement and empathy. At the same time developers have to be open to a process change and excited about the opportunity to contribute to code security in a meaningful way.

Moving security in with the development team, ensuring teams have the right mix of skills, and creating a collegial environment will go a long way toward a successful developer-first security effort.

## Learn more about DevSecOps

- [Best Practices for a DoD DevSecOps Culture](https://page.gitlab.com/resources-article-RightPlatformDoD.html)
- [DevSecOps with GitLab](https://about.gitlab.com/solutions/dev-sec-ops/)
- [Citizen-Centric Resiliency In Challenging Times](https://page.gitlab.com/webcast-citizen-centric-resiliency.html)

