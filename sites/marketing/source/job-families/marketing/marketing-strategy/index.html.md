---
layout: job_family_page
title: Marketing Strategy
---

The Marketing Strategy team at GitLab is responsible for developing marketing plans and go-to-market strategies. 

## Director, Marketing Strategy 
The Director, Marketing Strategy develops and executes a Global Marketing Strategy by working with Marketing Leadership and GTM teams including owning and driving key initiatives and projects. This role reports to [the Chief Marketing Officer](https://about.gitlab.com/job-families/marketing/chief-marketing-officer/).

### Responsibilities
* Own the overall measurement strategy for GitLab marketing, focusing on CWA/spend and impressions/spend
* Own the OKR process and reporting for marketing
* Partner with Marketing Leadership to formulate and develop regional/segment marketing plans and constantly evolving go-to-market strategies
* Partner with Marketing Operations to drive the right reporting and analytics for cross-marketing measurement
* Partner with Sales Strategy to integrate marketing and sales strategies into a cohesive GTM strategy
* Identify opportunities to improve go-to-market efficiencies and manage efforts to scale, align and invest in the business
* Examine and present recommendations to Marketing Leadership for alignment and investment in the desired state and opportunities for marketing efficiency
* Develop an understanding of marketing best practices and work with Marketing leadership to implement appropriate best practices
* Author and deliver high-impact presentations and plans to support CMO and CMO Staff in Board and E-Group level communications, as well as with the overall marketing team
* Assist with quarterly planning with Marketing Leadership and key GTM Leaders
* Develop and manage executive reporting on key metrics, formulate actionable insights and structure a concise, clear presentation of findings and prioritize issues as appropriate
* Be a trusted advisor to Marketing Leadership

### Requirements
* BA/BS degree, MBA or other related advanced degree preferred
* 8-10+ years of experience marketing operations and some experience in GTM or sales operations
* Excellent quantitative analytical skills, creativity in problem solving, and a keen business sense
* Ability to think strategically, but also have exceptional attention to detail to drive program management and execution
* Extensive track record of building high-quality spreadsheets, presentations and documents
* Superb analytical skills, technical aptitude and executive presence
* SaaS and B2B experience preferred
* Interest in GitLab, and open source software
* A shared interest in our values, and working in accordance with those values
* Capacity to thrive in a fully remote organization
* Leadership at GitLab
* Ability to use GitLab

### Job Grade
The Director, Marketing Strategy is a [grade #10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Performance Indicators
* [Pipe-to-spend per marketing activity](https://about.gitlab.com/handbook/marketing/performance-indicators/#net-new-business-pipeline-created)
* [Marketing efficiency ratio](https://about.gitlab.com/handbook/marketing/performance-indicators/#marketing-efficiency-ratio)
* [Total number of MQLs by month](https://about.gitlab.com/handbook/marketing/performance-indicators/#total-number-of-mqls-by-month)

## Career Ladder
The next step in the Marketing Strategy job family is not yet defined at GitLab.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to speak with the VP of Revenue Marketing. 
* Next, candidates will be invited to speak with the People Business Partner for Marketing. 
* Finally, candidates will be invited to interview with the Chief Marketing Officer. 

Additional details about our process can be found on our [hiring page](/handbook/hiring).
